# IRL Streaming with OBS

## Scope:

Primary puprpose is to document my BKM's and techniques when livestreaming. If I write any scripts they will populate here too, but for the most part it is taking notes on configurations. 

### Generic block diagram:

Most use-cases will be similar to this high-level summary

```mermaid
flowchart LR
    subgraph Mobile
        
        A["Camera"] -->B("Local Ingest")
        D["Microphone"] -->B
    end
    subgraph Home
    C[Desktop w/OBS]
    end
    C-->F("Commercial Ingest (Twitch)")
    B --> C
```

## Standard desktop OBS scene and filter configuration

This is a standard set of filters and scenes for OBS. Specifics may change, but fundamentially the same logic can apply even to a more classic gaming use-case of OBS.

### Scenes & Overlays

Overlays exist to give the viewers context, specifically TODO: put link to website.
How to setup? Document here!

### Audio Filters

There are a few core filters that are probably needed for every audio source: Compressor, Expander, Gain, Noise Reduction. Typically reasonable levels can carry between different microphone sources, but may need slight adjustment. While testing I talk both softly and excitedly into the microphone and watch my levels with the goal of hitting between -20 to -5dB. Adjust Gain while talking loudly: if the signal is saturated remove the Gain (and check other potential sources of gain like your microphone or Windows audio settings). If the signal is too quiet then you need Gain to increase the sound.



#### Compressor

#### Expander

#### Gain

#### Noise Reduction

### OBS Multi-Stream

## Use-Cases Index

- DSLR via Linux laptop
- Android device using Larix Broadcaster
- Insta360's Android app

### DSLR via Linux laptop

Used for local applications. An example is working in the garage, where I regularly relocate the tripod but always have WiFi coverage. The laptop _could_ publish directly to the commercial ingest server, however if anything goes ary (switching camera battery, WiFi drop) the entire stream 

```mermaid
flowchart LR
    subgraph On-Tripod
        
        A["DSLR (Sony ZV-1)"] -->|USB| B("gPhoto2")
        D["Microphone (Movo Wireless)"] -->|3.5mm|C(OBS)
        subgraph Laptop
            B --> C
        end
    end
    subgraph Desktop
    C -->|WiFi|E(OBS)
    end
    E-->|Internet|F("Commercial Ingest (Twitch)")
```

### Android device via IRL Pro

Can be used on mobile or WiFi networks. Relatively simple to setup with low gear requirements. Largest hurdle is poor device support, for example the Pixel 4a 5G can broadcast but it does not support selecting secondary cameras and struggles with wireless 3.5mm microphones. 

```mermaid
flowchart LR
    subgraph Mobile
        
        A["Android Phone with IRL Pro App"] 
        D["Earpods"] -->|Bluetooth| A
    end
    subgraph Stationary
    C[Desktop w/OBS]
    end
    C-->F("Commercial Ingest (Twitch)")
    A -->|WiFi or Cellular|C
```

There are multiple apps available with a similar feature suite. Larix Broadcaster is an excellent choice but recently moved to a subscription model. 
